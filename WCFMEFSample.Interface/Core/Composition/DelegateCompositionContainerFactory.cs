﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition.Hosting;

namespace WCFMEFSample.Interface
{
    /// <summary>
    /// Define a container factory
    /// </summary>
    public class DelegateCompositionContainerFactory:ICompositionContainerFactory
    {
        private Func<ExportProvider[], CompositionContainer> _factory;

        public DelegateCompositionContainerFactory(Func<ExportProvider[],CompositionContainer> factory)
        {
            if (factory == null)
                throw new ArgumentNullException("factory");

            _factory = factory;
        }

        public CompositionContainer CreateCompositionContainer(params ExportProvider[] providers)
        {
            return _factory(providers);
        }
    }
}
