﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition.Hosting;

namespace WCFMEFSample.Interface
{
    public interface ICompositionContainerFactory
    {
        CompositionContainer CreateCompositionContainer(params ExportProvider[] providers);
    }
}
