﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;

namespace WCFMEFSample.Interface
{
    /// <summary>
    /// Define an export provider
    /// </summary>
    public class ServiceHostExportProvider:ExportProvider
    {
        protected override IEnumerable<Export> GetExportsCore(ImportDefinition definition, AtomicComposition atomicComposition)
        {
            throw new NotImplementedException();
        }
    }
}
