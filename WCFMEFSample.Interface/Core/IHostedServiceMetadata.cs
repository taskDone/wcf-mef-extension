﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCFMEFSample.Interface
{
    public interface IHostedServiceMetadata
    {
        /// <summary>
        /// Get the name of the service
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Get the service type
        /// </summary>
        Type ServiceType { get; }
    }
}
