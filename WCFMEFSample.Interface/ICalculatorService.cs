﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace WCFMEFSample.Interface
{
    [ServiceContract(Namespace=Constants.Namespace)]
    public interface ICalculatorService:IHostedService
    {
        [OperationContract]
        int Add(int operandA, int operandB);
    }
}
